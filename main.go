package main

import "github.com/urfave/negroni"

func main() {
	//gorm
	initialMigration()
	//router
	router := NewRouter()
	//negroni Middlware
	n := negroni.Classic()
	n.UseHandler(router)
	n.Run(":8080")
}
