package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

//UserWeather struct
type UserWeather struct {
	User    User
	Weather Weather
}

//JSON
func getJSONBodyData(w http.ResponseWriter, r *http.Request) User {
	var user User
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	fmt.Println(string(body))
	if err := json.Unmarshal(body, &user); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) //unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}
	fmt.Println(user)
	return user
}

//Home handlefunc
func Home(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Home Endpoint Hit")

}

//AddUser handlefunc
func AddUser(w http.ResponseWriter, r *http.Request) {
	user := getJSONBodyData(w, r)

	addUser(&user)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(user); err != nil {
		panic(err)
	}
}

//EditUser handlefunc
func EditUser(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Edit User endpoint hit")
	user := getJSONBodyData(w, r)

	userID, _ := strconv.Atoi(mux.Vars(r)["id"])
	user.Model.ID = uint(userID)
	editUser(&user)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(user); err != nil {
		panic(err)
	}

}

//DeleteUser handlefunc
func DeleteUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Delete User Endpoint Hit")
	userID := mux.Vars(r)["id"]
	deleteUser(userID)
	fmt.Println("User deleted")

}

//ViewUsers handlefunc
func ViewUsers(w http.ResponseWriter, r *http.Request) {
	users := viewUsers()

	if err := json.NewEncoder(w).Encode(users); err != nil {
		panic(err)
	}
}

//ViewUser handlefunc
func ViewUser(w http.ResponseWriter, r *http.Request) {
	userID, _ := strconv.Atoi(mux.Vars(r)["id"])
	user := viewUser(userID)
	if user.ID == 0 {
		fmt.Fprintf(w, "No Users found")
	} else {

		if err := json.NewEncoder(w).Encode(user); err != nil {
			panic(err)
		}
	}
}

//ViewUserWeather handlefunc
func ViewUserWeather(w http.ResponseWriter, r *http.Request) {
	var userWeather UserWeather
	userID, _ := strconv.Atoi(mux.Vars(r)["id"])
	user := viewUser(userID)
	if user.ID == 0 {
		fmt.Fprintf(w, "No Users found")
	} else {
		userWeather.User = user
		userWeather.Weather = getWeatherData(&user)
		if err := json.NewEncoder(w).Encode(userWeather); err != nil {
			panic(err)
		}
	}
}

//ViewUsersWeather handlefunc
func ViewUsersWeather(w http.ResponseWriter, r *http.Request) {
	var weatherList []UserWeather
	users := viewUsers()
	for _, user := range users {
		weather := getWeatherData(&user)
		userWeather := UserWeather{user, weather}
		weatherList = append(weatherList, userWeather)
	}

	if err := json.NewEncoder(w).Encode(weatherList); err != nil {
		panic(err)
	}
}
