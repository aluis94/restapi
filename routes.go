package main

import "net/http"

//Route struct
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

//Routes slice
type Routes []Route

var routes = Routes{
	Route{
		"Home",
		"GET",
		"/",
		Home,
	},
	Route{
		"AddUser",
		"POST",
		"/user/add",
		AddUser,
	},
	Route{
		"EditUser",
		"PUT",
		"/user/edit/{id}",
		EditUser,
	},
	Route{
		"DeleteUser",
		"DELETE",
		"/user/delete/{id}",
		DeleteUser,
	},
	Route{
		"ViewUsers",
		"GET",
		"/users/view",
		ViewUsers,
	},
	Route{
		"ViewUser",
		"GET",
		"/user/view/{id}",
		ViewUser,
	},
	Route{
		"ViewUsersWeather",
		"GET",
		"/users/view/weather",
		ViewUsersWeather,
	},
	Route{
		"ViewUserWeather",
		"GET",
		"/user/view/{id}/weather",
		ViewUserWeather,
	},
}
