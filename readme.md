# Go RestAPI
1. [Overview](#overview)
   1. [Third-party-packages](#third-party-packages)
   2. [WeatherAPI](#weatherapi)
2. [Usage](#usage)
    1. [Add User](#add-user)
    2. [Edit User](#edit-user)
    3. [Delete User](#delete-user)
    4. [View Users](#view-users)
    5. [VIew Users with Weather Info](#view-users-with-weather-info)

---

## Overview
A rest API that allows the addition, modification, viewing, and deletion of users. It also connects to an external weather service to provide weather information for a user's chosen city.

Stores a user's name, city, and country code



### Third-party-packages

**Object relational mapping:**

github.com/jinzhu/gorm

**SQLite3 driver:**

github.com/mattn/go-sqlite3

**Gorilla MUX:**

github.com/gorilla/mux

**Negroni MiddleWare:**

github.com/urfave/negroni

### WeatherAPI
 The OpenWeather API is used for the external weather service.

 https://openweathermap.org/api

 Examples on how to use the API:

 https://openweathermap.org/current

---
## Usage
### Add User
**Method: POST**

**Path:** 
```
/user/add
```

**Sample Body**: 
``` 
{"Name":"Albert","City":"irvine","CountryCode":"US"}
```
### Edit User
**Method: PUT**

**Path:** 
```
/user/edit/{user_id}
```

**Sample Body**: 
```
{"Name":"Albert","City":"irvine","CountryCode":"US"}
```

### Delete User
**Method: DELETE**

**Path:** 
```
/user/delete/{user_id}
```
### View Users
**Method: GET**

#### All Users
**Path:** 
```
/users/view
```
#### Single User by ID
**Path:** 
```
/user/view/{user_id}
```
### View Users with Weather Info

#### All Users
**Path:** 
```
/users/view/weather
```
#### Single User by ID
**Path:** 
```
/user/view/{user_id}/weather
```

---

