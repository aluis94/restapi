package main

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"
)

//User Struct
type User struct {
	gorm.Model
	Name        string
	City        string
	CountryCode string
}

// gorm: initial migration
func initialMigration() {
	db, err := gorm.Open("sqlite3", "user.db")
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&User{})
}

//Add user
func addUser(user *User) {
	db, err := gorm.Open("sqlite3", "user.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	db.Create(user)
	fmt.Println("user created")
}

//editUser
func editUser(user *User) {
	db, err := gorm.Open("sqlite3", "user.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	var dbUser User
	db.Where("id = ?", user.Model.ID).Find(&dbUser)

	//update user data
	dbUser.Name = user.Name
	dbUser.City = user.City
	dbUser.CountryCode = user.CountryCode
	if dbUser.Model.ID != 0 {
		db.Save(&dbUser)
		fmt.Println("Successfully Updated User")
	} else {
		fmt.Println("User does not exist")
	}

}

//delete user

func deleteUser(id string) {
	db, err := gorm.Open("sqlite3", "user.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	var user User
	db.Where("id = ?", id).Find(&user)
	db.Unscoped().Delete(&user)

	fmt.Println("Successfully Deleted User")
}

//view al users

func viewUsers() []User {
	db, err := gorm.Open("sqlite3", "user.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()
	var users []User
	db.Find(&users)
	fmt.Println("{}", users)
	return users
}

//view single user by ID
func viewUser(id int) User {
	var user User
	db, err := gorm.Open("sqlite3", "user.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()
	db.Where("id = ?", id).Find(&user)
	return user
}
